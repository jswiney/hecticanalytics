//
//  File.swift
//  
//
//  Created by James Swiney on 18/1/20.
//

import Foundation
import UIKit
import HecticAnalyticsNetworking

class AnalyticEventSubmitter {
    
    private var queuedEvents = [HecticEvent]()
    private var timer : Timer?
    let deviceId : String
    private var observers = [NSObjectProtocol]()
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    
    init(deviceId:String) {
        self.deviceId = deviceId
        addNotifications()
        startTimer()
    }
    
    deinit {
        observers.forEach({ NotificationCenter.default.removeObserver($0) })
    }
    
    public func queue(event:HecticEvent) {
        
        guard let _ = HecticAnalytics.shared.token else {
            print("Token not set for Hectic Analytics")
            return
        }
        
        self.queuedEvents.append(event)
    }
    
    private func addNotifications() {
        let active = NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: nil) { (notification) in
            if let timer = self.timer, timer.isValid {
                return
            }
            self.startTimer()
        }
        observers.append(active)
        
        let inactive = NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: nil) { (notification) in
            if let timer = self.timer, timer.isValid {
                timer.invalidate()
                self.timer = nil
                self.submitEvents()
            }
        }
        observers.append(inactive)
    }
    
    private func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 30, repeats: true, block: { (timer) in
            self.submitEvents()
        })
    }
    
    private func submitEvents() {
        if queuedEvents.count == 0 {
            return
        }
        let events = queuedEvents
        send(events: events)
        queuedEvents.removeAll()
    }
    
    private func send(events:[HecticEvent]) {
        
        guard let token = HecticAnalytics.shared.token else {
            print("Token not set for Hectic Analytics")
            return
        }
        
        registerBackgroundTask()
        
        var request = URLRequest(url: URL(string: "\(HecticAnalytics.shared.env.url)/event")!)
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let requestBody = HecticEventRequest(deviceId: deviceId, events: events)
        request.httpBody = try? JSONEncoder().encode(requestBody)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let data = data {
                print(String(data: data, encoding: .utf8)!)
            }
            self.endBackgroundTask()
        }.resume()
        
    }
    
    private func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
    }
    
    private func endBackgroundTask() {
        print("Background task ended.")
        if backgroundTask == .invalid {
            return
        }
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = .invalid
    }
}
