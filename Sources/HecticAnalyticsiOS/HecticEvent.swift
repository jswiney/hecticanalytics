//
//  File.swift
//  
//
//  Created by James Swiney on 18/1/20.
//

import Foundation
import UIKit
import HecticAnalyticsNetworking

public extension HecticEvent {
    
    static func firstInstall() -> HecticEvent {
        return .createWith(name: "first_install")
    }
    
    static func createWith(name:String) -> HecticEvent  {
        
        var region = "unknown"
        if let localeRegion = Locale.current.regionCode, let description = Locale(identifier: "en_AU").localizedString(forRegionCode: localeRegion) {
            region = description
        }
        var device = UIDevice.current.model
        #if targetEnvironment(macCatalyst)
        device = "Mac"
        #endif
        
        return HecticEvent(name: name, device: UIDevice.current.model, os: UIDevice.current.systemVersion, created: Date().timeIntervalSinceNow, region: region)
    }
    
}
