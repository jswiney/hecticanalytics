import Foundation
import HecticAnalyticsNetworking

public class HecticAnalytics {
    
    public enum Environment {
        case local,heroku
        
        var url : String {
            switch self {
            case .local:
                return "http://127.0.0.1:8080"
            case .heroku:
                return "https://hectic-analytics.herokuapp.com"
            }
        }
    }
    
    public var token : String?
    public var env : Environment = .heroku
    
    private static let deviceIdKey = "deviceId"
    
    public static let shared = HecticAnalytics()
    
    private let deviceId : String
    
    private let submitter : AnalyticEventSubmitter
    
    public var shouldLogFirstInstall : (()->(Bool))?
    
    init() {
        if let id = UserDefaults.standard.string(forKey: HecticAnalytics.deviceIdKey) {
            deviceId = id
            submitter = AnalyticEventSubmitter(deviceId: id)
        } else {
            let id = UUID().uuidString
            deviceId = id
            UserDefaults.standard.set(id, forKey: HecticAnalytics.deviceIdKey)
            submitter = AnalyticEventSubmitter(deviceId: id)
            logFirstInstall()
        }
        
    }
    
    func logFirstInstall() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            let shouldLog = self.shouldLogFirstInstall?() ?? true
            if shouldLog {
                self.log(event: .firstInstall())
            }
        }
    }
    
    public func log(event:HecticEvent) {
        submitter.queue(event: event)
    }
}
