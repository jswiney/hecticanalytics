import XCTest

import HecticAnalyticsiOSTests

var tests = [XCTestCaseEntry]()
tests += HecticAnalyticsiOSTests.allTests()
XCTMain(tests)
